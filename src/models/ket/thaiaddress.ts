import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class ThaiAddresshModel {

  async thaiAddress(token:any, chwpart: any, amppart: any, tmbpart: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/address/select/${chwpart}/${amppart}/${tmbpart}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async thaiAddressFull(token:any, chwpart: any, amppart: any, tmbpart: any, moopart: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/address/select_full/${chwpart}/${amppart}/${tmbpart}/${moopart}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

}