import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class RerferServerModel {

    async select(hcode:any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referserver/select/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    // 'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectPhr() {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referserver/selectPhr`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    // 'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectReferboard() {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referserver/selectReferboard`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    // 'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
    async selectServer() {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referserver/selectServer`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    // 'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectUpload() {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referserver/selectUpload`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    // 'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}