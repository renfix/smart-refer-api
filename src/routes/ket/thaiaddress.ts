import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ThaiAddresshModel } from '../../models/ket/thaiaddress'

const thaiAddresshModel = new ThaiAddresshModel();
export default async function thaiaddress(fastify: FastifyInstance) {

    // select
    fastify.get('/select/:chwpart/:amppart/:tmbpart',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let chwpart = req.params.chwpart;
        let amppart = req.params.amppart;
        let tmbpart = req.params.tmbpart;
        try {
          let res_: any = await thaiAddresshModel.thaiAddress(token, chwpart, amppart, tmbpart);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_full/:chwpart/:amppart/:tmbpart/:moopart',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let chwpart = req.params.chwpart;
        let amppart = req.params.amppart;
        let tmbpart = req.params.tmbpart;
        let moopart = req.params.moopart;
        try {
            if(moopart){
                let res_: any = await thaiAddresshModel.thaiAddressFull(token, chwpart, amppart, tmbpart, moopart);
                reply.send(res_);      
            }else{
                let res_: any = await thaiAddresshModel.thaiAddress(token, chwpart, amppart, tmbpart);
                reply.send(res_);      
            }
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}