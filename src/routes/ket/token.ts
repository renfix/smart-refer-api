import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { TokensModel } from '../../models/ket/token'

const tokensModel = new TokensModel();
export default async function token(fastify: FastifyInstance) {

    // select
    fastify.get('/select/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
          let res_token: any = await tokensModel.getTokens(token, hcode);
          reply.send(res_token);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info_ = req.body;
        let info = {
          "hcode": info_.hcode,
          "line_name": info_.line_name,
          "line_token": info_.line_token
        }
        try {
          let res_token: any = await tokensModel.insert(token, info);
          reply.send(res_token);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        let info_ = req.body;
        let info = {
          "line_name": info_.line_name,
          "line_token": info_.line_token
        }
        try {
          let res_token: any = await tokensModel.update(token, hcode, info);
          reply.send(res_token);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}