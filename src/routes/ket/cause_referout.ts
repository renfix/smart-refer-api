import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CauseReferoutModel } from '../../models/ket/cause_referout'

const causeReferoutModel = new CauseReferoutModel();
export default async function causeReferback(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await causeReferoutModel.causeReferout(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }        
    })


}